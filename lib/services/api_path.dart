class APIPath {
  static String profile(String uid, String profileId) => 'users/$uid/profile/$profileId';
  static String profiles(String uid) => 'users/$uid/profile';
  static String medical(String uid, String medicalId) => 'users/$uid/medical/$medicalId';
  static String medicals(String uid) => 'users/$uid/medical';
  // Already Edited.
  static String medicals2() => 'satunol/klinik1/medical';
  static String medical2(String uid) => 'satunol/klinik1/medical/$uid';
  static String users() => 'satunol/klinik1/pasiens';
  static String user(String uid) => 'satunol/klinik1/pasiens/$uid';
  static String tests() => 'satunol/klinik1/profile';
  static String test(String uid) => 'satunol/klinik1/profile/$uid';
  static String reseps() => 'satunol/klinik1/resep';
  static String resep(String pasienId) => 'satunol/klinik1/resep/$pasienId';
  static String vital(String vitalId) => 'satunol/klinik1/vital/$vitalId';
  static String vitals() => 'satunol/klinik1/vital';
  // static String medicals2() => 'medical';
  static String users2() => 'users';
  static String user2(String uid) => 'users/$uid';
  // static String tests() => 'test';
  // static String test(String uid) => 'test/$uid';
  // static String medical2(String uid) => 'medical/$uid';  
}