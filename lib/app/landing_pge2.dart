import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:olohealth/app/home/perawat/perawat_page.dart';
import 'package:olohealth/app/home/profile/profile_page.dart';
import 'package:olohealth/app/sign_in/sign_in_page.dart';
import 'package:olohealth/services/auth.dart';
import 'package:olohealth/services/database.dart';
import 'package:provider/provider.dart';

import 'home/profile/doctor_page.dart';

class SelectionPage extends StatefulWidget {
  @override
  _SelectionPageState createState() => _SelectionPageState();
}

class _SelectionPageState extends State<SelectionPage> {
  String userId;

  @override
  void initState() {
    super.initState();
    getCurrentUser().then((user) => {
          setState(() {
            if (user != null) {
              userId = user.uid;
            }
          }),
        });
    // widget.auth.onAuthStateChanged.listen((user) {
    //   print('my id is - ${user?.uid}');
    // });
  }

  // Future<User> _checkUserId() async {
  //   User user = await widget.auth.currentUser();
  //   _updateUser(user);
  //   return user;
  // }

  // void _updateUser(User user) {
  //   setState(() {
  //     _user = user;
  //   });
  // }

  Future<FirebaseUser> getCurrentUser() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    return user;
  }

  @override
  Widget build(BuildContext context) {
    final database = Provider.of<Database>(context, listen: false);
    if (userId != null) {
      print(userId + ' not null');
      return StreamBuilder<User>(
        stream: database.readUser(userId: userId),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.active) {
            final User user = snapshot.data;
            final userRole = user?.role ?? 'null';
            if (user == null) {
              return SignInPage.create(context);
            }
            if (userRole == 'admin') {
              return Provider<Database>(
                create: (_) => FirestoreDatabase(uid: userId),
                child: ProfilePage(),
              );
            }
            if (userRole == 'doctor') {
              return Provider<Database>(
                create: (_) => FirestoreDatabase(uid: userId),
                child: DoctorPage(),
              );
            }
            if (userRole == 'perawat') {
              return Provider<Database>(
                create: (_) => FirestoreDatabase(uid: userId),
                child: PerawatPage(),
              );
            }
          }
          return Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        },
      );
    }
    return CircularProgressIndicator();
  }
}
