// import 'package:Klinikonline/app/home/home_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../services/auth.dart';
import '../services/database.dart';

class SelectedPage extends StatelessWidget {
  const SelectedPage({Key key, this.user, this.database}) : super(key: key);
  final User user;
  final Database database;

  Future<String> inputData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final String uid = user.uid.toString();
    print(uid);
  return uid;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        title: Text('userRole'),
      ),
      body: _buildContent(context),
    );
    // return StreamBuilder<User>(
    //     stream: database.readUser(uid: user.uid),
    //     builder: (context, snapshot) {
    //       final user = snapshot.data;
    //       final userRole = user?.role ?? 'null';
    //       return Scaffold(
    //         appBar: AppBar(
    //           elevation: 2.0,
    //           title: Text(userRole),
    //         ),
    //         body: _buildContent(),
    //       );
    //     });
  }

  Widget _buildContent(BuildContext context) {
    final database = Provider.of<Database>(context, listen: false);
    // print(uid);
    return StreamBuilder<User>(
        stream: database.readUser(userId: 'test'),
        builder: (context, snapshot) {
          final user = snapshot.data;
          final userRole = user?.role ?? 'null';
          // print(user.uid);
          return Column(
            children: [
              Text(userRole),
            ],
          );
        });
  }
}
