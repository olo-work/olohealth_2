import 'package:flutter/material.dart';
import 'email_sign_in_form_change.dart';
import './../sign_in/arc_clipper.dart';

class EmailSignInPage extends StatelessWidget {
  EmailSignInPage({
    Key key,
    this.image,
    this.showIcon = true,
  }) : super(key: key);
  final image;
  final showIcon;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      child: Stack(
        children: <Widget>[
          new Column(
            children: <Widget>[
              topHalf(context),
              bottomHalf,
            ],
          ),
          new Container(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: EmailSignInFormChange.create(context),
              ),
            ),
          )
        ],
      ),
    );
    // return Scaffold(
    //   appBar: AppBar(
    //     title: Text(
    //       'Login Email',
    //       textAlign: TextAlign.center,
    //       ),
    //     elevation: 4.0,
    //   ),
    //   // body: _buildContent(context),
    //   body: Stack(
    //     children: <Widget>[
    //       new Container(
    //         decoration: BoxDecoration(
    //           image: DecorationImage(
    //             image: AssetImage('images/suster.png'),
    //             fit: BoxFit.cover,
    //           ),
    //         ),
    //       ),
    //     new Padding(
    //       padding: const EdgeInsets.all(16.0),
    //       child: Card(
    //         child: EmailSignInFormChange.create(context),
    //       ),
    //     )
    //     ]
    //   ),
    // );
  }

  Widget topHalf(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    return new Flexible(
      flex: 2,
      child: ClipPath(
        clipper: new ArcClipper(),
        child: Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                  colors: <Color>[
                    Colors.blueGrey.shade800,
                    Colors.black87,
                  ],
                ),
              ),
            ),
            showIcon
                ? Scaffold(
                  backgroundColor: Colors.transparent,
                    body: new Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: deviceSize.height / 8,
                            width: deviceSize.width / 2,
                            child: Image.asset('images/pratama.png'),
                          ),
                          Text(
                            'Klinik Pratama',
                            // overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              color: Colors.blue[300],
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            'GKI Pondok Indah',
                            // overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              color: Colors.blue[300],
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : new Container(
                    width: double.infinity,
                    child: image != null
                        ? Image.asset(
                            image,
                            fit: BoxFit.cover,
                          )
                        : new Container())
          ],
        ),
      ),
    );
  }

  final bottomHalf = new Flexible(
    flex: 3,
    child: new Container(),
  );
}
