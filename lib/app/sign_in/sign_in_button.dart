import 'package:flutter/cupertino.dart';

import '../../common_widgets/costum_raised_button.dart';

class SignInButton extends CustomRaisedButton {
  SignInButton({
    @required String text,
    Color color,
    Color textColor,
    VoidCallback onPressed,
    double height,
    double borderRadius,
  }) :
  assert(text != null), 
  super (
    child: Text(
      text,
      style: TextStyle(color: textColor, fontSize: 15.0),
    ),
    color: color,
    onPressed: onPressed,
    height: height,
    borderRadius: borderRadius,
  );
}