import 'dart:async';
import 'package:olohealth/app/home/models/pasien.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:olohealth/app/home/models/resep.dart';
import 'package:olohealth/app/sign_in/sign_in_button.dart';
import 'package:olohealth/common_widgets/platform_alert_dialog.dart';
import 'package:olohealth/common_widgets/platform_exeption_alert_dialog.dart';
import 'package:olohealth/services/database.dart';

class SetResepPage extends StatefulWidget {
  const SetResepPage(
      {Key key, @required this.database, this.resep, this.pasien})
      : super(key: key);
  final Database database;
  final Resep resep;
  final Pasien pasien;

  static Future<void> show(BuildContext context,
      {Database database, Pasien pasien}) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => SetResepPage(
          database: database,
          pasien: pasien,
        ),
        fullscreenDialog: true,
      ),
    );
  }

  static Future<void> show2(
      {BuildContext context,
      Database database,
      Resep resep,
      Pasien pasien}) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => SetResepPage(
            database: database, resep: resep, pasien: pasien),
        fullscreenDialog: true,
      ),
    );
  }

  @override
  _SetResepPageState createState() => _SetResepPageState();
}

class _SetResepPageState extends State<SetResepPage> {
  final _formKey = GlobalKey<FormState>();
  // DateTime _end;
  String _obat1;
  String _obat2;
  String _obat3;
  String _obat4;
  String _obat5;
  String _obat6;
  

  @override
  void initState() {
    super.initState();
    if (widget.resep != null) {
      _obat1 = widget.resep.obat1;
      _obat2 = widget.resep.obat2;
      _obat3 = widget.resep.obat3;
      _obat4 = widget.resep.obat4;
      _obat5 = widget.resep.obat5;
      _obat6 = widget.resep.obat6;
    }
  }

  bool _saveForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<void> _submit() async {
    print('My pasienID: ${widget.pasien?.id}');
    if (_saveForm()) {
      try {
        final medicals = await widget.database.resepStream().first;
        final allNames = medicals.map((e) => e.obat1).toList();
        if (widget.resep != null) {
          allNames.remove(widget.resep.obat1);
        }
        if (allNames.contains(_obat1)) {
          PlatformAlertDialog(
            title: 'name already used',
            content: 'please use a diffrent resep names',
            defaultActionText: 'OK',
          ).show(context);
        } else {
          final id = widget.resep?.id ?? documentIdFromCurrentDate();
          final lastDate = widget.resep?.resepDate ?? DateTime.now();
          final pasienId = widget.pasien.id;
          final resep = Resep(
            id: id,
            pasienId: pasienId,
            resepDate: lastDate,
            // end: _end,
            obat1: _obat1,
            obat2: _obat2,
            obat3: _obat3,
            obat4: _obat4,
            obat5: _obat5,
            obat6: _obat6,
          );
          await widget.database.setResep(resep);
          Navigator.of(context).pop();
        }
      } on PlatformException catch (e) {
        PlatformExeptionAlertDialog(
          title: 'operation failed',
          exception: e,
        ).show(context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        title: Text(
            widget.pasien == null ? 'Buat Rekam Medis' : 'Edit Rekam Medis'),
        // actions: <Widget>[
        //   FlatButton(
        //     child: Text(
        //       'Save',
        //       style: TextStyle(fontSize: 18, color: Colors.white),
        //     ),
        //     onPressed: _submit,
        //   ),
        // ],
      ),
      // body: Stack(
      //   children: <Widget>[
      //     new Container(
      //       decoration: BoxDecoration(
      //         image: DecorationImage(
      //           image: AssetImage('images/suster.png'),
      //           fit: BoxFit.cover,
      //         ),
      //       ),
      //     ),
      //     new Container(
      //       child: _buildContent(),
      //     ),
      //   ],
      // ),
      body: _buildContent(),
      backgroundColor: Colors.grey[200],
    );
  }

  Widget _buildContent() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: _buildForm(),
          ),
        ),
      ),
    );
  }

  _buildForm() {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: _buildFormChildren(),
      ),
    );
  }

  _buildFormChildren() {
    return [
      TextFormField(
        decoration: InputDecoration(labelText: 'Obat 1'),
        initialValue: _obat1,
        validator: (value) =>
            value.isNotEmpty ? null : 'Obat 1 tidak boleh kosong',
        onSaved: (value) => _obat1 = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Obat 2'),
        initialValue: _obat2,
        // validator: (value) => value.isNotEmpty ? null : 'Keluhan Utama tidak boleh kosong',
        onSaved: (value) => _obat2 = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Obat 3'),
        initialValue: _obat3,
        onSaved: (value) => _obat3 = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Obat 4'),
        initialValue: _obat4,
        onSaved: (value) => _obat4 = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Obat 5'),
        initialValue: _obat5,
        onSaved: (value) => _obat5 = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Obat 6'),
        initialValue: _obat6,
        onSaved: (value) => _obat6 = value,
      ),
      SizedBox(height: 10.0),
      SignInButton(
        text: 'Done',
        textColor: Colors.white,
        color: Colors.blue[500],
        onPressed: _submit,
        height: 50,
        borderRadius: 10,
      ),
    ];
  }
}
