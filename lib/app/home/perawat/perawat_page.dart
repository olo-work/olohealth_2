import 'package:olohealth/app/home/models/pasien.dart';
import 'package:olohealth/app/home/perawat/vitalsign_page.dart';
import 'package:olohealth/app/home/profile/list_items_builder.dart';
import 'package:olohealth/app/home/profile/pasien_list.dart';
import 'package:olohealth/app/home/profile/pasien_page.dart';
import 'package:olohealth/common_widgets/platform_exeption_alert_dialog.dart';
import 'package:olohealth/services/database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import '../../../services/auth.dart';
import '../models/profile.dart';

class PerawatPage extends StatelessWidget {
  const PerawatPage({
    Key key,
    this.profile,
    this.user,
    this.database,
    this.pasien,
  }) : super(key: key);
  final AuthBase user;
  final Profile profile;
  final Database database;
  final Pasien pasien;

  Future<void> _signOut(BuildContext context) async {
    try {
      final auth = Provider.of<AuthBase>(context, listen: false);
      await auth.signOut();
    } catch (e) {
      print(e.toString());
    }
  }

  // Future<void> _confirmSignOut(BuildContext context) async {
  //   final didRequestSignout = await PlatformAlertDialog(
  //     title: 'Log Out',
  //     content: 'are you sure you want to Logout',
  //     cancelAction: 'Cancel',
  //     defaultActionText: 'Logout',
  //   ).show(context);
  //   if (didRequestSignout == true) {
  //     _signOut(context);
  //   }
  // }

  // Future<void> _delete(BuildContext context, User user) async {
  //   try {
  //     final database = Provider.of<Database>(context, listen: false);
  //     await database.deleteUser(user);
  //   } on PlatformException catch (e) {
  //     PlatformExeptionAlertDialog(
  //       title: 'operation failed',
  //       exception: e,
  //     ).show(context);
  //   }
  // }

  Future<void> _delete2(BuildContext context, Pasien pasien) async {
    try {
      final database = Provider.of<Database>(context, listen: false);
      await database.deletePasien(pasien);
    } on PlatformException catch (e) {
      PlatformExeptionAlertDialog(
        title: 'operation failed',
        exception: e,
      ).show(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<AuthBase>(context, listen: false);
    return FutureBuilder<User>(
      future: auth.currentUser(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final user = snapshot.data;
          final perawatId = user?.uid;
          print('Perawat Id: ' + perawatId);
          if (perawatId.isNotEmpty) {
            final database = Provider.of<Database>(context, listen: false);
            return StreamBuilder<User>(
              stream: database.readUser(userId: perawatId),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final _user = snapshot.data;
                  final adminName = _user?.name;
                  final avatar = _user?.avatar;
                  return Scaffold(
                    extendBodyBehindAppBar: false,
                    backgroundColor: Colors.black54,
                    appBar: PreferredSize(
                      preferredSize: Size.fromHeight(
                          MediaQuery.of(context).size.height / 4),
                      child: AppBar(
                        flexibleSpace: FlexibleSpaceBar(
                          stretchModes: <StretchMode>[
                            StretchMode.zoomBackground,
                            StretchMode.blurBackground,
                            StretchMode.fadeTitle,
                          ],
                          centerTitle: true,
                          title: Center(
                            child: Column(
                              children: [
                                SizedBox(height: 25),
                                Image.asset(
                                  '$avatar',
                                  fit: BoxFit.cover,
                                  height:
                                      MediaQuery.of(context).size.height * 0.1,
                                ),
                                // SizedBox(height: 8),
                                Text(
                                  adminName,
                                  style: TextStyle(
                                    fontSize: 22,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        elevation: 2.0,
                        backgroundColor: Colors.black54,
                        // title: Text(adminName),
                        actions: <Widget>[
                          FlatButton(
                            child: Text(
                              'Log out',
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.white,
                              ),
                            ),
                            onPressed: () => _signOut(context),
                          )
                        ],
                      ),
                    ),
                    body: Stack(
                      children: [
                        new Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('images/doctor.png'),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        new Container(
                          color: Colors.black54,
                          constraints: BoxConstraints.expand(),
                        ),
                        _buildContents(context),
                      ],
                    ),
                    // floatingActionButton: FloatingActionButton(
                    //   child: Icon(Icons.person_add),
                    //   onPressed: () => SetPasienPage.show(
                    //     context,
                    //     database:
                    //         Provider.of<Database>(context, listen: false),
                    //   ),
                    // ),
                  );
                }
                return CircularProgressIndicator();
              },
            );
          }
          return CircularProgressIndicator();
        }
        return CircularProgressIndicator();
      },
    );
  }

  Widget _buildContents(BuildContext context) {
    final database = Provider.of<Database>(context, listen: false);
    final int query = new DateTime(
      DateTime.now().year,
      DateTime.now().month,
      DateTime.now().day,
    ).millisecondsSinceEpoch;
    // print(query);
    return StreamBuilder<List<Pasien>>(
      stream: database.queryPasien(date: query),
      builder: (context, snapshot) {
        return ListItemsBuilder<Pasien>(
          snapshot: snapshot,
          itemBuilder: (context, pasien) => Dismissible(
            key: Key('pasien-${pasien.id}'),
            background: Container(color: Colors.red),
            direction: DismissDirection.endToStart,
            onDismissed: (direction) => _delete2(context, pasien),
            child: PasienList(
              user: pasien,
              onTap: () => VitalPage.show(
                  context, pasien),
            ),
          ),
        );
      },
    );
  }
}
