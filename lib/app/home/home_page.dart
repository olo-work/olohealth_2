import 'dart:async';
import 'package:olohealth/app/home/profile/list_items_builder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './../../common_widgets/platform_alert_dialog.dart';
import './../../services/auth.dart';
import './../../services/database.dart';
import './models/profile.dart';
// import './profile/add_sexslave_page.dart';
// import './profile/profile_file.dart';

class HomePage extends StatelessWidget {
  Future<void> _signOut(BuildContext context) async {
    try {
      final auth = Provider.of<AuthBase>(context, listen: false);
      await auth.signOut();
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> _confirmSignOut(BuildContext context) async {
    final didRequestSignout = await PlatformAlertDialog(
      title: 'Log Out',
      content: 'are you sure you want to Logout',
      cancelAction: 'Cancel',
      defaultActionText: 'Logout',
    ).show(context);
    if (didRequestSignout == true) {
      _signOut(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
        actions: <Widget>[
          FlatButton(
            child: Text(
              'Log out',
              style: TextStyle(
                fontSize: 18.0,
                color: Colors.white,
              ),
            ),
            onPressed: () => _confirmSignOut(context),
          )
        ],
      ),
      body: _buildContents(context),
    );
  }

  Widget _buildContents(BuildContext context) {
    final database = Provider.of<Database>(context, listen: false);
    return StreamBuilder<List<Profile>>(
      stream: database.readSlave(),
      builder: (context, snapshot) {
        return ListItemsBuilder<Profile>(
          snapshot: snapshot,
          itemBuilder: (context, profile) {
            return InkWell(
              onTap: () => {},
              child: Image.asset(
                'images/SM_logo.png',
                fit: BoxFit.cover,
                height: 300,
              ),
            );
          },
        );
      },
    );
  }
}
