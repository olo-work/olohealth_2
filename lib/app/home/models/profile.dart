import 'package:meta/meta.dart';

class Profile {
  Profile({
    @required this.id,
    @required this.uid,
    @required this.name,
    @required this.gender,
    @required this.age,
    @required this.adress,
    @required this.phone,
    @required this.handphone,
    @required this.pekerjaan,
    @required this.alergi,
    @required this.ttl,
    @required this.templahir,
    @required this.avatar,
  });

  final String id;
  final String uid;
  final String name;
  final String gender;
  final int age;
  final String adress;
  final String phone;
  final String handphone;
  final String pekerjaan;
  final String alergi;
  final String ttl;
  final String templahir;
  final String avatar;

  factory Profile.fromMap(Map<String, dynamic> data, String documentId) {
    if (data == null) {
      return null;
    }
    final String uid = data['uid'];
    final String name = data['name'];
    final String gender = data['gender'];
    final int age = data['age'];
    final String adress = data['adress'];
    final String phone = data['phone'];
    final String handphone = data['handphone'];
    final String pekerjaan = data['pekerjaan'];
    final String alergi = data['alergi'];
    final String ttl = data['ttl'];
    final String templahir = data['templahir'];
    final String avatar = data['avatar'];
    return Profile(
      uid: uid,
      id: documentId,
      name: name,
      gender: gender,
      age: age,
      adress: adress,
      phone:phone,
      handphone: handphone,
      pekerjaan: pekerjaan,
      alergi: alergi,
      ttl: ttl,
      templahir: templahir,
      avatar: avatar,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'name': name,
      'gender': gender,
      'age': age,
      'adress': adress,
      'phone': phone,
      'handphone': handphone,
      'pekerjaan': pekerjaan,
      'alergi': alergi,
      'ttl': ttl,
      'templahir': templahir,
      'avatar': avatar,
    };
  }
}
