class Pasien {
  Pasien({
    this.id,
    this.uid,
    this.name,
    this.gender,
    this.age,
    this.adress,
    this.phone,
    this.handphone,
    this.pekerjaan,
    this.alergi,
    this.ttl,
    this.templahir,
    this.avatar,
    this.idPasien,
    this.appoDate,
    this.queryDate,
    this.withDoc,
    this.vitalId,
  });

  final String id;
  final String uid;
  final String name;
  final String gender;
  final double age;
  final String adress;
  final String phone;
  final String handphone;
  final String pekerjaan;
  final String alergi;
  final DateTime ttl;
  final String templahir;
  final String avatar;
  final DateTime appoDate;
  final DateTime queryDate;
  final String withDoc;
  final String idPasien;
  String vitalId = 'default';

  double get _age => DateTime.now().difference(ttl).inDays.toDouble() / 365;

  factory Pasien.fromMap(Map<String, dynamic> data, String documentId) {
    if (data == null) {
      print('data null');
      return null;
    }
    // print('data not null');
    final String uid = data['uid'];
    final String name = data['name'];
    final String idPasien = data['idPasien'];
    final String gender = data['gender'];
    final double age = data['age'];
    final String adress = data['adress'];
    final String phone = data['phone'];
    final String handphone = data['handphone'];
    final String pekerjaan = data['pekerjaan'];
    final String alergi = data['alergi'];
    final int ttlMiliSeconds = data['ttl'];
    final String templahir = data['templahir'];
    final String avatar = data['avatar'];
    final int appoMiliSeconds = data['appoDate'];
    final int queryDate = data['queryDate'];
    final String vitalId = data['vitalId'];
    final String withDoc = data['withDoc'];
    // print('vitalId : ' + vitalId);
    return Pasien(
      uid: uid,
      idPasien: idPasien,
      id: documentId,
      name: name,
      gender: gender,
      age: age,
      adress: adress,
      phone: phone,
      handphone: handphone,
      pekerjaan: pekerjaan,
      alergi: alergi,
      ttl: DateTime.fromMillisecondsSinceEpoch(ttlMiliSeconds),
      templahir: templahir,
      avatar: avatar,
      appoDate: DateTime.fromMillisecondsSinceEpoch(appoMiliSeconds),
      queryDate: DateTime.fromMillisecondsSinceEpoch(queryDate),
      vitalId: vitalId,
      withDoc: withDoc,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'idPasien': idPasien,
      'name': name,
      'gender': gender,
      'age': _age,
      'adress': adress,
      'phone': phone,
      'handphone': handphone,
      'pekerjaan': pekerjaan,
      'alergi': alergi,
      'ttl': ttl.millisecondsSinceEpoch,
      'templahir': templahir,
      'avatar': avatar,
      'appoDate': appoDate.millisecondsSinceEpoch,
      'queryDate': queryDate.millisecondsSinceEpoch,
      'withDoc': withDoc,
      'vitalId': vitalId,
    };
  }

  Map<String, dynamic> toMap2() {
    return {
      'vitalId': vitalId,
    };
  }
}
