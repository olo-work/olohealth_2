class Vital {
  Vital({
    this.id,
    this.pasienId,
    this.berat,
    this.tinggi,
    this.bmi,
    this.tekanandarah,
    this.nadi,
    this.respiration,
    this.spo2,
    this.temp,
    this.gula,
    this.lastCheck,
  });
  final String id;
  final String pasienId;
  final double berat;
  final double tinggi;
  final double bmi;
  final String tekanandarah;
  final int nadi;
  final double respiration;
  final double spo2;
  final double temp;
  final double gula;
  final DateTime lastCheck;

  factory Vital.fromMap(Map<String, dynamic> data, String documentId) {
    if (data == null) {
      return null;
    }
    final int intDate = data['lastCheck'];
    return Vital(
      id: documentId,
      pasienId: data['pasienId'],
      berat: data['berat'],
      tinggi: data['tinggi'],
      bmi: data['bmi'],
      tekanandarah: data['tekanandarah'],
      nadi: data['nadi'],
      respiration: data['respiration'],
      spo2: data['spo2'],
      temp: data['temp'],
      gula: data['gula'],
      lastCheck: DateTime.fromMillisecondsSinceEpoch(intDate),
    ); 
  }

   Map<String, dynamic> toMap() {
     return<String, dynamic>{
       'pasienId': pasienId,
       'berat': berat,
       'tinggi': tinggi,
       'bmi': bmi,
       'tekanandarah': tekanandarah,
       'nadi': nadi,
       'respiration': respiration,
       'temp': temp,
       'spo2': spo2,
       'gula': gula,
       'lastCheck': lastCheck.millisecondsSinceEpoch, 
     };
   }
   Map<String, dynamic> updateMap() {
     return<String, dynamic>{
       'pasienId': pasienId, 
     };
   }
}
