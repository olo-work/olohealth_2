class Resep {
  Resep({
    this.id,
    this.resepDate,
    this.obat1,
    this.obat2,
    this.obat3,
    this.obat4,
    this.obat5,
    this.obat6,
    this.pasienId,
  });
  String id;
  DateTime resepDate;
  String obat1;
  String obat2;
  String obat3;
  String obat4;
  String obat5;
  String obat6;
  String pasienId;

  factory Resep.fromMap(Map<dynamic, dynamic> value, String documentId) {
    if (value == null) {
      return null;
    }
    final int dateValue = value['resepDate'];
    return Resep(
      id: documentId,
      pasienId: value['pasienId'],
      resepDate: DateTime.fromMillisecondsSinceEpoch(dateValue),
      obat1: value['obat1'],
      obat2: value['obat2'],
      obat3: value['obat3'],
      obat4: value['obat4'],
      obat5: value['obat5'],
      obat6: value['obat6'],
    );
  }
  Map<String, dynamic> toMap() {
      return <String, dynamic>{
        'pasienId': pasienId,
        'resepDate': resepDate.millisecondsSinceEpoch,
        'obat1': obat1,
        'obat2': obat2,
        'obat3': obat3,
        'obat4': obat4,
        'obat5': obat5,
        'obat6': obat6,
      };
    }
}
