import 'package:olohealth/app/home/medical/format.dart';
import 'package:olohealth/app/home/models/entry.dart';
import 'package:olohealth/app/home/models/pasien.dart';
import 'package:flutter/material.dart';

class MedicalListItem extends StatelessWidget {
  const MedicalListItem({
    @required this.pasien,
    @required this.medical,
    @required this.onTap,
  });

  final Pasien pasien;
  final Medical medical;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    // DateTime rekamDate = DateTime.parse(medical.id);
    return Container(
        height: MediaQuery.of(context).size.height * 0.472,
        child: InkWell(
          onTap: onTap,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0, vertical: 4.0),
            child: _buildContents(context),
          ),
        ),
      );
    // return Container(
    //   // constraints: BoxConstraints.expand(
    //   //   height: MediaQuery.of(context).size.height * 0.8,
    //   //   width: MediaQuery.of(context).size.width,
    //   // ),
    //   child: Stack(
    //     children: [
    //       new Container(
    //         decoration: BoxDecoration(
    //           image: DecorationImage(
    //             image: AssetImage('images/doctor.png'),
    //             fit: BoxFit.cover,
    //           ),
    //         ),
    //       ),
    //       new Container(
    //         color: Colors.black54,
    //         // constraints: BoxConstraints.expand(
    //         //   // height: MediaQuery.of(context).size.height,
    //         //   // width: MediaQuery.of(context).size.width,
    //         // ),
    //       ),
    //       InkWell(
    //         onTap: onTap,
    //         child: Container(
    //           constraints: BoxConstraints.expand(),
    //           margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
    //           child: _buildContents(context),
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }

  Widget _buildContents(BuildContext context) {
    final date = Format.date(medical.lastCheckUp);
    final hours = TimeOfDay.fromDateTime(medical.lastCheckUp).format(context);
    return Card(
      child: Stack(
        children: [
          Container(
            color: Colors.black.withOpacity(0.7),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  // SizedBox(width: 30),
                  Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Tanggal Periksa', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Keluhan Utama', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Keluhan lain', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Palpasi', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Perkusi', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Auskultasi', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Inspeksi', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Working Diagnose', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Difrential Diagnose', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Injeksi', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text('Terapi', style: TextStyle(fontSize: 20, color: Colors.white)),
                        // Text('Resep Dokter', style: TextStyle(fontSize: 20, color: Colors.white)),
                      ],
                    ),
                  ),
                  SizedBox(width: 30),
                  Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(': $date, $hours', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${medical.keluhan1}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${medical.keluhan2}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${medical.palpasi}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${medical.perkusi}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${medical.auskultasi}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${medical.inspeksi}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${medical.diagnose1}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${medical.diagnose2}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${medical.injeksi}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        Text(': ${medical.terapi}', style: TextStyle(fontSize: 20, color: Colors.white)),
                        // Text(': ${medical.resep}', style: TextStyle(fontSize: 20, color: Colors.white)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DismissibleMedicalListItem extends StatelessWidget {
  const DismissibleMedicalListItem({
    this.key,
    this.pasien,
    this.medical,
    this.onDismissed,
    this.onTap,
  });

  final Key key;
  final Pasien pasien;
  final Medical medical;
  final VoidCallback onDismissed;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      background: Container(color: Colors.red),
      key: key,
      direction: DismissDirection.endToStart,
      onDismissed: (direction) => onDismissed(),
      child: MedicalListItem(
        pasien: pasien,
        medical: medical,
        onTap: onTap,
      ),
    );
  }
}

class DismissibleMedicalList extends StatelessWidget {
  const DismissibleMedicalList({
    this.key,
    this.pasien,
    this.medical,
    this.onDismissed,
    this.onTap,
  });

  final Key key;
  final Pasien pasien;
  final Medical medical;
  final VoidCallback onDismissed;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      background: Container(color: Colors.red),
      key: key,
      direction: DismissDirection.endToStart,
      onDismissed: (direction) => onDismissed(),
      child: MedicalListItem(
        pasien: pasien,
        medical: medical,
        onTap: onTap,
      ),
    );
  }
}
