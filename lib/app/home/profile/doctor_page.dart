import 'package:date_format/date_format.dart';
import 'package:olohealth/app/home/medical/job_entries_page.dart';
import 'package:olohealth/app/home/models/pasien.dart';
import 'package:olohealth/app/home/profile/list_items_builder.dart';
import 'package:olohealth/app/home/profile/pasien_list.dart';
import 'package:olohealth/common_widgets/platform_exeption_alert_dialog.dart';
import 'package:olohealth/services/database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import '../../../services/auth.dart';
import '../models/profile.dart';
import 'dart:async';

class DoctorPage extends StatelessWidget {
  DoctorPage({
    Key key,
    this.profile,
    this.user,
    this.database,
    this.pasien,
    this.auth,
  }) : super(key: key);
  final User user;
  final Profile profile;
  final Database database;
  final Pasien pasien;
  final AuthBase auth;

  Future<void> _signOut(BuildContext context) async {
    try {
      final auth = Provider.of<AuthBase>(context, listen: false);
      await auth.signOut();
    } catch (e) {
      print(e.toString());
    }
  }

  // Future<void> _confirmSignOut(BuildContext context) async {
  //   final didRequestSignout = await PlatformAlertDialog(
  //     title: 'Log Out',
  //     content: 'are you sure you want to Logout',
  //     cancelAction: 'Cancel',
  //     defaultActionText: 'Logout',
  //   ).show(context);
  //   if (didRequestSignout == true) {
  //     _signOut(context);
  //   }
  // }

  // Future<void> _delete(BuildContext context, User user) async {
  //   try {
  //     final database = Provider.of<Database>(context, listen: false);
  //     await database.deleteUser(user);
  //   } on PlatformException catch (e) {
  //     PlatformExeptionAlertDialog(
  //       title: 'operation failed',
  //       exception: e,
  //     ).show(context);
  //   }
  // }

  Future<void> _delete2(BuildContext context, Pasien pasien) async {
    try {
      final database = Provider.of<Database>(context, listen: false);
      await database.deletePasien(pasien);
    } on PlatformException catch (e) {
      PlatformExeptionAlertDialog(
        title: 'operation failed',
        exception: e,
      ).show(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<AuthBase>(context, listen: false);
    return FutureBuilder<User>(
      future: auth.currentUser(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final user = snapshot.data;
          final userId = user?.uid;
          print('doctor id: ' + userId);
          if (userId.isNotEmpty) {
            final database = Provider.of<Database>(context, listen: false);
            return StreamBuilder<User>(
              stream: database.readUser(userId: '$userId'),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final _user = snapshot.data;
                  final userName = _user?.name;
                  final avatar = _user?.avatar;
                  print(avatar);
                  return Scaffold(
                    extendBodyBehindAppBar: true,
                    backgroundColor: Colors.black54,
                    appBar: PreferredSize(
                      preferredSize: Size.fromHeight(200.0),
                      child: AppBar(
                        flexibleSpace: FlexibleSpaceBar(
                          stretchModes: <StretchMode>[
                            StretchMode.zoomBackground,
                            StretchMode.blurBackground,
                            StretchMode.fadeTitle,
                          ],
                          centerTitle: true,
                          title: Center(
                            child: Text(
                              'Appointment',
                              style: TextStyle(
                                fontSize: 32,
                              ),
                            ),
                          ),
                          // background: Stack(
                          //   fit: StackFit.expand,
                          //   children: [
                          //     Image.asset(
                          //       'images/profile.png',
                          //       fit: BoxFit.cover,
                          //     ),
                          //     const DecoratedBox(
                          //       decoration: BoxDecoration(
                          //         gradient: LinearGradient(
                          //           begin: Alignment(0.0, 0.5),
                          //           end: Alignment(0.0, 0.0),
                          //           colors: <Color>[
                          //             Color(0x60000000),
                          //             Color(0x00000000),
                          //           ],
                          //         ),
                          //       ),
                          //     ),
                          //   ],
                          // ),
                        ),
                        leading: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.asset(
                            avatar,
                            fit: BoxFit.cover,
                          ),
                        ),
                        elevation: 2.0,
                        backgroundColor: Colors.black54,
                        title: Text(userName),
                        actions: <Widget>[
                          FlatButton(
                            child: Text(
                              'Log out',
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.white,
                              ),
                            ),
                            onPressed: () => _signOut(context),
                          )
                        ],
                      ),
                    ),
                    body: Stack(
                      children: [
                        new Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('images/doctor.png'),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        new Container(
                          color: Colors.black54,
                          constraints: BoxConstraints.expand(),
                        ),
                        _buildContents(context, userId),
                      ],
                    ),
                    // floatingActionButton: FloatingActionButton(
                    //   child: Icon(Icons.person_add),
                    //   onPressed: () => SetPasienPage.show(
                    //     context,
                    //     database: Provider.of<Database>(context, listen: false),
                    //     // pasien: pasien,
                    //   ),
                    // ),
                  );
                }
                return Scaffold(
                  body: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              },
            );
          }
        }
        return Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  Widget _buildContents(BuildContext context, String uid) {
    final database = Provider.of<Database>(context, listen: false);
    final int query = new DateTime(
      DateTime.now().year,
      DateTime.now().month,
      DateTime.now().day,
    ).millisecondsSinceEpoch;
    print(query);
    return StreamBuilder<List<Pasien>>(
      stream: database.streamPasien(doctor: uid, date: query),
      builder: (context, snapshot) {
        print(snapshot.data);
        return ListItemsBuilder<Pasien>(
          snapshot: snapshot,
          itemBuilder: (context, pasien) => Dismissible(
            key: Key('pasien-${pasien.id}'),
            background: Container(color: Colors.red),
            direction: DismissDirection.endToStart,
            onDismissed: (direction) => _delete2(context, pasien),
            child: PasienList(
              user: pasien,
              onTap: () => ProfileMedicalPage.show(context, pasien),
            ),
          ),
        );
      },
    );
  }
}
