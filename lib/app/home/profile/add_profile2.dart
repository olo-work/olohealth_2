import 'dart:async';
import 'package:olohealth/services/auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../common_widgets/platform_alert_dialog.dart';
import '../../../common_widgets/platform_exeption_alert_dialog.dart';
import '../../../services/database.dart';
import '../../sign_in/sign_in_button.dart';
import '../models/profile.dart';

class SetProfilePage extends StatefulWidget {
  const SetProfilePage({Key key, @required this.database, this.profile, this.user})
      : super(key: key);
  final Database database;
  final Profile profile;
  final User user;

  static Future<void> show(BuildContext context, {Database database, Profile profile}) async {
    await Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => SetProfilePage(
            database: database,
            profile: profile,
          ),
          fullscreenDialog: true,
        ),
      );
  }
  static Future<void> show2({BuildContext context, Database database, Profile profile, User user}) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) =>
            SetProfilePage(database: database, profile: profile, user: user),
        fullscreenDialog: true,
      ),
    );
  }

  @override
  _SetProfilePageState createState() => _SetProfilePageState();
}

class _SetProfilePageState extends State<SetProfilePage> {
  final _formKey = GlobalKey<FormState>();
  String _name;
  String _gender;
  int _age;
  String _adress;
  String _phone;
  String _handphone;
  String _pekerjaan;
  String _alergi;
  String _ttl;
  String _templahir;
  String _avatar;

  @override
  void initState() {
    super.initState();
    if (widget.profile != null) {
      _name = widget.profile.name;
      _gender = widget.profile.gender;
      _age = widget.profile.age;
      _adress = widget.profile.adress;
      _phone = widget.profile.phone;
      _handphone = widget.profile.handphone;
      _pekerjaan = widget.profile.pekerjaan;
      _alergi = widget.profile.alergi;
      _ttl = widget.profile.ttl;
      _templahir = widget.profile.templahir;
      _avatar = widget.profile.avatar;
    }
  }

  bool _saveForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<void> _submit() async {
    if (_saveForm()) {
      try {
        final profiles = await widget.database.testStream().first;
        final allNames = profiles.map((e) => e.name).toList();
        if (widget.profile != null) {
          allNames.remove(widget.profile.name);
        }
        if (allNames.contains(_name)) {
          PlatformAlertDialog(
            title: 'name already used',
            content: 'please use a diffrent profile names',
            defaultActionText: 'OK',
          ).show(context);
        } else {
          final id = widget.profile?.id ?? documentIdFromCurrentDate();
          final uid = widget.user.uid;
          final profile = Profile(
            id: id,
            uid: uid,
            name: _name,
            adress: _adress,
            gender: _gender,
            age: _age,
            phone: _phone,
            handphone: _handphone,
            pekerjaan: _pekerjaan,
            alergi: _alergi,
            ttl: _ttl,
            templahir: _templahir,
            avatar: _avatar,
          );
          await widget.database.setTest(profile);
          Navigator.of(context).pop();
        }
      } on PlatformException catch (e) {
        PlatformExeptionAlertDialog(
          title: 'operation failed',
          exception: e,
        ).show(context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        title: Text(widget.profile == null ? 'Biodata' : 'Edit Biodata'),
        // actions: <Widget>[
        //   FlatButton(
        //     child: Text(
        //       'Save',
        //       style: TextStyle(fontSize: 18, color: Colors.white),
        //     ),
        //     onPressed: _submit,
        //   ),
        // ],
      ),
      // body: Stack(
      //   children: <Widget>[
      //     new Container(
      //       decoration: BoxDecoration(
      //         image: DecorationImage(
      //           image: AssetImage('images/suster.png'),
      //           fit: BoxFit.cover,
      //         ),
      //       ),
      //     ),
      //     new Container(
      //       child: _buildContent(),
      //     ),
      //   ],
      // ),
      body: _buildContent(),
      backgroundColor: Colors.grey[200],
    );
  }

  Widget _buildContent() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: _buildForm(),
          ),
        ),
      ),
    );
  }

  _buildForm() {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: _buildFormChildren(),
      ),
    );
  }

  _buildFormChildren() {
    return [
      TextFormField(
        decoration: InputDecoration(labelText: 'Nama'),
        initialValue: _name,
        validator: (value) => value.isNotEmpty ? null : 'Name not be empty',
        onSaved: (value) => _name = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Umur'),
        initialValue: _age != null ? '$_age' : null,
        onSaved: (value) => _age = int.tryParse(value) ?? 0,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Jenis Kelamin - Pria/Wanita'),
        initialValue: _gender,
        onSaved: (value) => _gender = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Alamat Rumah'),
        initialValue: _adress,
        onSaved: (value) => _adress = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Tempat Lahir'),
        initialValue: _templahir,
        onSaved: (value) => _templahir = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Tanggal Lahir'),
        initialValue: _ttl,
        onSaved: (value) => _ttl = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'No Telp Rumah'),
        initialValue: _phone,
        onSaved: (value) => _phone = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'no Handphone'),
        initialValue: _handphone,
        onSaved: (value) => _handphone = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Pekerjaan'),
        initialValue: _pekerjaan,
        onSaved: (value) => _pekerjaan = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'alergi'),
        initialValue: _alergi,
        onSaved: (value) => _alergi = value,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Avatar Url'),
        initialValue: _avatar,
        onSaved: (value) => _avatar = value,
      ),
      SizedBox(height: 10.0),
      SignInButton(
        text: 'Done',
        textColor: Colors.white,
        color: Colors.indigo[500],
        onPressed: _submit,
        height: 50,
        borderRadius: 10,
      ),
    ];
  }
}
