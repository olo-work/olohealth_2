import 'package:olohealth/app/home/medical/format.dart';
import 'package:olohealth/app/home/models/pasien.dart';
import 'package:flutter/material.dart';

class PasienList extends StatelessWidget {
  const PasienList({Key key, @required this.user, this.onTap})
      : super(key: key);

  final Pasien user;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    // return ListTile(
    //   title: Text(sexslave.name),
    //   onTap: onTap,

    // );
    return Card(
      color: Colors.black12,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          child: _buildText(context),
          onTap: onTap,
        ),
      ),
    );
  }

  Widget _buildText(BuildContext context) {
    final ageInt = user.age.toStringAsFixed(0);
    final appoDate = Format.date(user.appoDate);
    final dayOfWeek = Format.dayOfWeek(user.appoDate);
    final startTime = TimeOfDay.fromDateTime(user.appoDate).format(context);
    return ListTile(
      leading: Image.asset(
        '${user.avatar}',
        fit: BoxFit.cover,
      ),
      title: Text(
        '${user.name} | ${user.gender} | $ageInt',
        style: TextStyle(
          fontSize: 24,
          color: Colors.white,
        ),
      ),
      subtitle: Row(
        children: [
          Text(
            'Have a appointment at',
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            dayOfWeek,
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            appoDate,
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            startTime,
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
            ),
          ),
          // SizedBox(width: 20,),
          // Text(
          //   'with Doctor -',
          //   style: TextStyle(
          //     fontSize: 18,
          //     color: Colors.white,
          //   ),
          // ),
          // SizedBox(
          //   width: 10,
          // ),
          // Text(
          //   '${user.withDoc}',
          //   style: TextStyle(
          //     fontSize: 18,
          //     color: Colors.white,
          //   ),
          // ),
          // SizedBox(
          //   width: 10,
          // ),
        ],
      ),
    );
  }
}
