import 'package:flutter/material.dart';

import '../models/profile.dart';

class ProfileList extends StatelessWidget {
  const ProfileList({Key key, @required this.profile, this.onTap})
      : super(key: key);

  final Profile profile;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    // return ListTile(
    //   title: Text(sexslave.name),
    //   onTap: onTap,

    // );
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          child: _buildText(),
          onTap: onTap,
        ),
      ),
    );
  }

  Widget _buildText() {
    return ListTile(
      title: Text('${profile.name}'),
      subtitle: Text('${profile.id}'),
    );
  }
}
