import 'package:flutter/material.dart';

class CustomOutlineButton extends StatelessWidget {
  CustomOutlineButton({
    this.child,
    this.color,
    this.onColor,
    this.borderRadius: 10.0,
    this.height: 50.0,
    this.onPressed,
  }): assert(borderRadius != null);
  final Widget child;
  final Color color;
  final Color onColor;
  final double borderRadius;
  final double height;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      child: OutlineButton(
              child: child,
              disabledBorderColor: color,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(borderRadius),
                ),
              ),
              onPressed: onPressed,
            ),
    );
  }
}