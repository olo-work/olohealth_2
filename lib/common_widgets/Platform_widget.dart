import 'dart:io';

import 'package:flutter/material.dart';

abstract class PlatformWidget extends StatelessWidget {
  Widget buildIOSAlert(BuildContext context);
  Widget buildAndroidAlert(BuildContext context);

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      return buildIOSAlert(context);
    } 
    return buildAndroidAlert(context);
  }
}